from pathlib import Path
from operator import itemgetter

import os

BYTES_IN_GB = 1024 * 1024 * 1024
BYTES_IN_MB = 1024 * 1024
BYTES_IN_KB = 1024


def convert_to_readable(dir_size):

    if dir_size < BYTES_IN_KB:
        return "%.2f" % dir_size, "B"
    elif dir_size < BYTES_IN_MB:
        return "%.2f" % float(dir_size / BYTES_IN_KB), "KB"
    elif dir_size < BYTES_IN_GB:
        return "%.2f" % float(dir_size / BYTES_IN_MB), "MB"
    else:
        return "%.2f" % float(dir_size / BYTES_IN_GB), "GB"


def get_top_size(dir_usage, param, top):

    # Sort elements in the list by passed parameter to look for
    ordered_list = sorted(dir_usage, key=itemgetter(param), reverse=True)

    # Return first top number of elements
    return ordered_list[:top]


def report_directory_usage(folders):

    for folder in folders:
        size, unit = convert_to_readable(folder['size'])
        dir_path = folder['path']
        dir_names = folder['directories']
        file_names = folder['files']
        print(f'\n[ {size} {unit} ] Path: `{dir_path}` \n\tDirectories: `{dir_names}` \n\t\tFiles: `{file_names}`')


def get_directory_usage(directory):

    # List of directory usage data
    dir_usage = []

    # Iterate through all folders and sub folders in home directory
    for dir_path, dir_names, file_names in os.walk(directory, followlinks=False):
        dir_size = 0
        for file in file_names:
            full_path = os.path.join(dir_path, file)
            if os.path.islink(full_path):
                continue
            dir_size += os.path.getsize(full_path)
        dir_usage.append({
            'path': dir_path,
            'size': dir_size,
            'directories': dir_names,
            'files': file_names
        })

    return dir_usage

# Get user home directory
home_dir = str(Path.home())

print(f'\n* File sizes are NOT `On Disk` size *')

dir_usage_data = get_directory_usage(home_dir)
print(f'\n>> Gathered directory usage data')

sorted_dir_usage_data = get_top_size(dir_usage_data, 'size', 3)
print(f'\n>> Sorted directory usage data')

report_directory_usage(sorted_dir_usage_data)
print(f'\n>> Report generated')
